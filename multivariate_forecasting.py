from numpy import genfromtxt
import numpy as np
import statsmodels.api as sm
import math

class MultivariateForecasting:
    def __init__(self, filename, steps, exog_file=None):
        self.filename = filename
        self.steps = steps
        self.predictions = []
        self.errors = []
        self.rmse = []

        self.data = genfromtxt(self.filename, delimiter=',')
        self.train = self.data.transpose()

        self.predictions = np.zeros((self.steps, len(self.train[0])))
        self.errors = np.zeros((self.steps, len(self.train[0])))
        self.rmse = np.zeros(len(self.errors[0]))

        if not exog_file == None:
            self.exog_file = exog_file



    def compute_rmse(self):
        for i in range(len(self.rmse)):
            self.rmse[i] = math.sqrt(sum(row[i]**2 for row in self.errors) / len(self.errors))

        print self.rmse
        return self.rmse

    # this is for 3 without exogenous variable
    def compute_varmax(self):

        # try to predict for n steps
        for i in range(self.steps):
            print "step: " + str(i)
            endog = self.train[0:30+i,:]

            mod = sm.tsa.VARMAX(endog, order=(1, 0), trend="nc")
            res = mod.fit()

            forecast = res.forecast(1)
            self.predictions[i] = forecast
            self.errors[i] = forecast - self.train[30+i,:]

        print self.predictions
        print "done - now compute for rmse"
        return self.compute_rmse()


    def compute_varmax_with_exog(self):

        exog_data = genfromtxt(self.exog_file, delimiter=',')
        if exog_data.ndim > 1:
            exog_data = exog_data.transpose()
        else:
            exog_data = np.array(exog_data)[np.newaxis]
            exog_data = exog_data.T

        for i in range(self.steps):
            print "step: " + str(i)
            endog = self.train[0:30+i,:]

            exog = exog_data[0:30+i,:]
            mod = sm.tsa.VARMAX(endog, order=(1, 0), trend="nc", exog=exog)
            res = mod.fit()

            exog_test = np.asarray([exog_data[30+i,:]])
            forecast = res.forecast(1, exog=exog_test)

            self.predictions[i] = forecast
            self.errors[i] = forecast - self.train[30+i,:]

        print self.predictions
        print "done - now compute for rmse"
        return self.compute_rmse()

forecast = MultivariateForecasting("cluster0/bpi2017_lt_50.csv", 20)
forecast.compute_varmax()
