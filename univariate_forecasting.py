from numpy import genfromtxt
import numpy as np
import statsmodels.api as sm
import math

class UnivariateForecasting:
    def __init__(self, filename, steps):
        self.filename = filename
        self.steps = steps
        self.predictions = []
        self.errors = []
        self.rmse = []

        self.train = genfromtxt(self.filename, delimiter=',')

        self.predictions = np.zeros(self.steps)
        self.errors = np.zeros(self.steps)
        self.rmse = 0


    def compute_rmse(self):
        self.rmse = math.sqrt(sum(err**2 for err in self.errors) / len(self.errors))

        print self.rmse
        return self.rmse

    def compute_arma(self):

        # try to predict for n steps
        for i in range(self.steps):
            print "step: " + str(i)
            train_data = self.train[0:30+i]

            res = sm.tsa.ARMA(train_data, order=(1, 0)).fit()

            forecast = res.forecast(1)
            self.predictions[i] = forecast[0]
            self.errors[i] = forecast[0] - self.train[i+30]

        print self.predictions
        print "done - now compute for rmse"
        return self.compute_rmse()


forecast1 = UnivariateForecasting("cluster35/bpi2011_lt_50_2.csv", 20)
res1 = forecast1.compute_arma()

forecast2 = UnivariateForecasting("cluster35/bpi2011_lt_50_2.csv", 20)
res2 = forecast2.compute_arma()

print "printing the final rmse results for univariate predictions"

print res1
print res2

