from numpy import genfromtxt
import numpy as np
import statsmodels.api as sm
import pandas as pd
from dateutil import parser
from datetime import datetime, timedelta

def encode_log():
    print "in encode log function"
    data = genfromtxt('2004visitdetails.csv', delimiter=',', dtype=None, names=True)
    departments = []
    times = []


    # read all departments
    # get earliest time and latest time
    earliestTime = None
    latestTime = None
    for row in data:
        dept = row["entry_group"]
        if not dept in departments:
            departments.append(dept)

        # make the time series per day firsty
        time = datetime.strptime(row["entry_date"], '"%m/%d/%Y %I:%M:%S %p"').strftime("%Y/%m/%d") #.strftime("%m/%d/%y %H:00")
        if not time in times:
            times.append(time)


        # if earliestTime is None:
        #     earliestTime = time
        # else:
        #     if earliestTime.time() > time.time():
        #         earliestTime = time
        # if latestTime is None:
        #     latestTime = time
        # else:
        #     if latestTime.time() < time.time():
        #         latestTime = time

    times.sort()
    departments.sort()

    timeseries = np.zeros((len(departments), len(times)))
    timeseriesCount = np.zeros((len(departments), len(times)))
    exog = np.zeros((2, len(times)))
    exogCount = np.zeros((2, len(times)))

    for row in data:
        duration = row["duration_hours"]
        deptIndex = departments.index(row["entry_group"])
        timeIndex = times.index(datetime.strptime(row["entry_date"], '"%m/%d/%Y %I:%M:%S %p"').strftime("%Y/%m/%d"))
        timeseries[deptIndex][timeIndex] = timeseries[deptIndex][timeIndex] + duration
        timeseriesCount[deptIndex][timeIndex] = timeseriesCount[deptIndex][timeIndex] + 1

        age = row["age_years"]
        exog[0][timeIndex] = exog[0][timeIndex] + age
        exogCount[0][timeIndex] = exogCount[0][timeIndex] + 1

        gender = row["gender"]
        if gender == 1:
            exog[1][timeIndex] = exog[1][timeIndex] + 1
        exogCount[1][timeIndex] = exogCount[1][timeIndex] + 1


    for idx in range(len(departments)):
        for jindx in range(len(times)):
            if timeseriesCount[idx][jindx] > 0:
                timeseries[idx][jindx] = timeseries[idx][jindx]/timeseriesCount[idx][jindx]

    for indx in range(len(exogCount[0])):
        exog[0][indx] = exog[0][indx] / exogCount[0][indx]
        exog[1][indx] = exog[1][indx] / exogCount[1][indx]


    # generate time rows
    # time = []
    # while earliestTime <= latestTime:
    #     time.append(earliestTime)
    #     earliestTime = earliestTime.time() + timedelta(hours=1)

    np.set_printoptions(suppress=True)
    np.savetxt("encoded_values.csv", timeseries, delimiter=",")

    import csv

    myfile = open("encoded_values_dates.csv", 'wb')
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    wr.writerow(times)

    np.set_printoptions(suppress=True)
    np.savetxt("encoded_values_exog.csv", exog, delimiter=",")

    # myfile = open("encoded_values_exog.csv", 'wb')
    # wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    # wr.writerow(exog)

    print "done"

encode_log()