
from numpy import genfromtxt
import numpy as np
import statsmodels.api as sm
import pandas as pd
import math

def compute_varmax():
    dta = pd.read_stata('lutkepohl2.dta')

    dta.index = dta.qtr
    endog = dta.ix['1960-04-01':'1978-10-01', ['dln_inv', 'dln_inc', 'dln_consump']]

    exog = pd.Series(np.arange(len(endog)), index=endog.index, name='trend')
    exog = endog['dln_consump']

    train = endog[['dln_inv', 'dln_inc']]
    mod = sm.tsa.VARMAX(train, order=(2,0), trend='nc', exog=exog)

    res = mod.fit(maxiter=1000)
    #
    test = train[:-1]
    test = test[:-1]

    exog_test = np.random.normal(size=(1, mod.k_exog))
    # fr = res.forecast(1, endog=test)
    fr = res.forecast(1, exog=exog_test)

    print fr


def compute_var():
    # this part reads the encoded traces
    data = genfromtxt('5traindailynumeric.csv', delimiter=',')
    data = data.transpose()

    # this is the part that creates the model using vector auto-regression
    model = sm.tsa.VAR(data)
    dataColumns = len(data[0])

    # this part adds the maximum lag for the model
    results = model.fit(0)

    # this reads the test file,
    # using this test file does not give good predictions cause the vector size is different
    # testdata = genfromtxt('5testdailynumeric.csv', delimiter=',')
    # testdata = testdata.transpose()
    # test = np.delete(testdata, -1, 0)
    # # thinking of adding 0s to the vector just to satisfy the vector size problem (making predictions with 0 columns)
    # a = np.zeros(shape=(len(test),dataColumns-len(test[0])))
    # test = np.append(test, a, 1)

    # using the same training data to test cause they have the same vector size
    test = np.delete(data, -1, 0)
    test = np.delete(test, -1, 0)
    fr = results.forecast(test, 3)

# this is for 3 without exogenous variable
def compute_varmax_medical_time():
    data = genfromtxt('encoded_values.csv', delimiter=',')
    train = data.transpose()

    errors = np.zeros((100, 2))
    predictions = np.zeros((100, 2))
    # try to predict for 100 steps
    for i in range(100):
        print "step: " + str(i)
        endog = train[0:30+i,:]

        mod = sm.tsa.VARMAX(endog, order=(1, 0), trend="nc")
        res = mod.fit(maxiter=1000)
        print "done making the model"

        fr = res.forecast(1)
        predictions[i] = fr
        errors[i] = fr - train[i+30]

    print "done - now compute for rmse"
    rmse = np.zeros(len(errors[0]))
    rmse[0] = math.sqrt(sum(row[0]**2 for row in errors) / len(errors))
    rmse[1] = math.sqrt(sum(row[1]**2 for row in errors) / len(errors))

    print "done calculating rmse"
    print rmse


# this is for 3 without exogenous variable
def compute_varmax_next_activity():
    data = genfromtxt('12trainhospital_numeric_trimmed2.csv', delimiter=',', skip_header=1)
    train = data.transpose()

    trainDat = train[:,0:5].tolist()

    zeroes = train.max() * np.random.rand(30, 5)
    zeroes = zeroes.tolist()
    for row in trainDat:
        zeroes.append(row)

    # train = zeroes + train[:,0:5]

    train = np.asarray(zeroes)

    mod = sm.tsa.VARMAX(train, order=(1,0), trend="nc")
    # mod = sm.tsa.VARMAX(train, order=(1,0), trend="nc")

    res = mod.fit(maxiter=1000)

    print "done making the model"
    # fr = res.forecast(1, endog=test)
    fr = res.forecast(1)

    print fr

def compute_varmax_medical_time_with_exog():
    data = genfromtxt('encoded_values_52rows.csv', delimiter=',')
    train = data.transpose()

    data2 = genfromtxt('encoded_values_exog2.csv', delimiter=',')
    # exog = np.array(data2)[np.newaxis]
    # exog = exog.T
    exog = data2.transpose()

    mod = sm.tsa.VARMAX(train, order=(1,0), trend="nc", exog=exog)
    # mod = sm.tsa.VARMAX(train, order=(1,0), trend="nc")

    res = mod.fit(maxiter=1000)

    print "done making the model"
    # fr = res.forecast(1, endog=test)
    exog_test = [[45.1053540587219,0.454732510288066]]
    fr = res.forecast(1, exog=exog_test)

    print fr
# compute_var()
# compute_varmax()
compute_varmax_medical_time()
# compute_varmax_medical_time_with_exog()
# compute_varmax_next_activity()

def plot_data(ts_data):
    import datetime
    import matplotlib.pyplot as plt
    x = range(0, len(ts_data))
    y = ts_data

    plt.plot(x, y)
    plt.show()